//============================================================================
// Name        : motionlogic.cpp
// Author      : Reza
// Version     :
// Copyright   : Your copyright notice
// Description : C++, Ansi-style

//============================================================================

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>

using namespace std;

//Default path and file name for passing parameters to scripts
#define DEF_PATH "/home/reza/workspace/motionlogic/scripts/cmd"

//Default directory for daemon
#define CURRENT_DIR "."
#define INTERVAL_DELAY 10

int main(void)
{
    int pid, sid;

    pid = fork();
    if(pid < 0){
        perror("FORK ERROR");
        exit(0);
    }

    if(pid > 0)
    	exit(1);

    umask(0);

    sid = setsid();
    if(sid < 0){
        perror("SET SID ERROR");
        exit(3);
    }

    if(chdir(CURRENT_DIR) < 0)
    {
        perror(CURRENT_DIR);
        exit(4);
    }

    // Endless deamon's loop
    while(1)
    {
		string line;
		ifstream myfile (DEF_PATH);

		if (myfile.is_open())
		{
			getline (myfile,line);
			//cout << "command : " << line <<endl;
			system(line.c_str());
			myfile.close();

			if( remove( DEF_PATH ) != 0 )
				perror( "Error deleting cmd file" );
			else
				puts( "cmd file successfully deleted" );

		}
		else
			cout << "cmd file was not uploaded!"<<'\n';

        sleep(INTERVAL_DELAY); // Wait INTERVAL_DELAY seconds
    }
    return EXIT_SUCCESS;
}
